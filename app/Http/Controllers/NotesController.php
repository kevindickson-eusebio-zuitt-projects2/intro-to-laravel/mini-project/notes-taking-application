<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Notes;

use Auth;

class NotesController extends Controller
{
    //

    public function __construct(){

    	$this->middleware('auth')->except(['index']);
        // $this->middleware('is.notes.owner')->except(['index','create']);
        // ->except() no need authentication to access.

    }
	 // Create a new post

    // Endpoint: GET /posts/create

	 public function create(){

		return view('note.create');

	}

	// Endpoint: Post /posts

    public function store(Request $req){

    	// Creating a new post object

    	$new_note = new Notes([

    		'content' => $req->input('content'),

    		'user_id' => Auth::user()->id

    	]);

    	// Save it to the database

    	$new_note->save();

    	// Redirect the user somewhere

    	return redirect('/notes/create');

    }

     // Endpoint: GET /posts

    public function index(){

    	$note_list = Notes::all();

    	return view("note.index")->with('notes', $note_list);
    	// with-> for passing the identifier to the index.blade.php
    }

    // Endpoint: delete /notes/<notes_id>

    public function destroy($notes_id)
    {
        // Find the existing post to be deleted
        $existing_notes = Notes::find($notes_id);
        // Delete the post
        $existing_notes->delete();
        // Redirect the user somewhere
        return redirect('/notes');
    }

     // Endpoint: GET /posts/<posts_id>/edit

    public function edit($notes_id){

        // Find the post to be updated
        $existing_notes = Notes::find($notes_id);

        return view('note.edit')->with('note',$existing_notes);
        // Redirect the user to page whhere the pst will be updated
    }

    // Endpoint: PUT /posts/<posts_id>

    public function update($notes_id, Request $req){

        // Finding existing post to be udpated
        $existing_notes = Notes::find($notes_id);

        // Set the new values of an existing post
        $existing_notes->content = $req->input('content');

        $existing_notes->save(); //to save
        // Redirect thee user to the page of individual post

        return redirect("/notes");

    }

     // Endpoint: GET /posts/my-posts

    public function myNotes(){

        $my_notes = Auth::user()->notes;

        return view("note.myown")->with('notes', $my_notes);
    }
}
