<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Note

Route::get('/notes/create', 'NotesController@create');

Route::post('/notes', 'NotesController@store');

Route::get('/notes', 'NotesController@index');

Route::delete('/notes/{notes_id}', 'NotesController@destroy');

Route::get('/notes/{notes_id}/edit','NotesController@edit');

Route::put('/notes/{notes_id}/edit','NotesController@update');

Route::get('/posts/my-notes','NotesController@myNotes'); // custom method