@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">
            <h1>My notes</h1>
        </div>

      <div class="col-md-8">
          @foreach ($notes as $note)
             {{-- @if($post->is_active) --}}
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
               
                <h3 class="card-title" id="user_creator"><small>Created by:</small> {{$note->user->name}}</h3>
                  
                  <h2 class="card-title ">{{ $note->content }}</h2>

                  @if(Auth::check() && (Auth::user()->id === $note->user_id))
                  <div class="btnActions mt-2">
                    <div class="row">
                      <div class="col-md-6 d-flex align-items-center">
                         <h6 class="card-text">
                          <small>Created:</small> <strong>{{$note->created_at}}</strong>
                          {{-- <a id="{{$note->id}}" href="/posts/{{$note->id}}"></a> --}}
                        </h6>
                      </div>
                      <div class="col-md-6 d-flex justify-content-end">
                          <a href="/notes/{{$note->id}}/edit" class="btn btn-primary mx-1">Edit</a>
                          <form method="POST" action="/notes/{{ $note->id }}">
                          @method('DELETE')

                          @csrf
                         
                          <button type="submit" class="btn btn-danger mx-1">Delete</button>
                          </form>
                      </div>

                    </div>
                  </div>
                  @endif

                </div>
              </div>
            </div>
           {{-- @endif --}}
          @endforeach

    </div>
</div>

@endsection